#!/bin/bash
################################################################
# Author:  Magnus Kronnäs
# license: GNU GENERAL PUBLIC LICENSE Version 3
#
# Copyright (c) 2018 Magnus Kronnäs
################################################################

# Common values and functions

# Style
## From https://gist.github.com/bcap/5682077#file-terminal-control-sh by Bruno "Polaco" Penteado
## Terminal output control (http://www.termsys.demon.co.uk/vtansi.htm)

TC="\e["

Bold="${TC}1m"   
Undr="${TC}4m"   
Inv="${TC}7m"    
Reg="${TC}22;24m"
RegF="${TC}39m"
RegB="${TC}49m"
Rst="${TC}0m"
Black="${TC}30m";
Red="${TC}31m";
Green="${TC}32m";
Yellow="${TC}33m";
Blue="${TC}34m"; 
Purple="${TC}35m";
Cyan="${TC}36m";
White="${TC}37m";

# Config

MESSAGE_TIME=1;
CONFIG_PATH="$HOME/.config/sgs";
CONFIG_FILE="$CONFIG_PATH/config.yml"

# Aiding functions

msgNewLine() {
	if [ $LOG -lt 1 ]
    then
        printf "\n";
    fi
};

## Print an headline and waits
msgHeadline(){
	if [ $SHOW_TIME -lt 1 ]
	then
	   TIME=`date +'%H:%m:%S'`;
	   TIME_COLOR="$White$Bold[$TIME]$Rst ";
	   TIME_NO_COLOR="[$TIME] ";
	else
	   TIME_COLOR="";
	   TIME_NO_COLOR="";
	fi
	
    if [ $LOG -lt 1 ]
    then
        printf "$TIME_COLOR$White$Bold$1$Rst\n";
        sleep $MESSAGE_TIME;
    else
        printf "$TIME_NO_COLOR$1\n";
    fi
};

## Print an importent message
msgImportent() {
    if [ $SHOW_TIME -lt 1 ]
	then
	   TIME=`date +'%H:%m:%S'`;
	   TIME_COLOR="$White$Bold[$TIME]$Rst ";
	   TIME_NO_COLOR="[$TIME] ";
	else
	   TIME_COLOR="";
	   TIME_NO_COLOR="";
	fi
	
    if [ $LOG -lt 1 ]
    then
        printf "$TIME_COLOR$Red$Bold$1$Rst;msgWait\n";
    else
        printf "$TIME_NO_COLOR$1\n";
    fi
};

## Print an OK message
msg() {
    if [ $SHOW_TIME -lt 1 ]
	then
	   TIME=`date +'%H:%m:%S'`;
	   TIME_COLOR="$White$Bold[$TIME]$Rst ";
	   TIME_NO_COLOR="[$TIME] ";
	else
	   TIME_COLOR="";
	   TIME_NO_COLOR="";
	fi
	
    if [ $LOG -lt 1 ]
    then
        printf "$TIME_COLOR$Green$1$Rst\n";
    else
        printf "$TIME_NO_COLOR$1\n";
    fi
};

## Print an informativ message
msgInfo() {
    if [ $SHOW_TIME -lt 1 ]
	then
	   TIME=`date +'%H:%m:%S'`;
	   TIME_COLOR="$White$Bold[$TIME]$Rst ";
	   TIME_NO_COLOR="[$TIME] ";
	else
	   TIME_COLOR="";
	   TIME_NO_COLOR="";
	fi
	
	if [ $LOG -lt 1 ]
    then
        printf "$TIME_COLOR$Yellow$1$Rst\n";
    else
        printf "$TIME_NO_COLOR$1\n";
    fi
};

# Parse argument to script
## Frome https://stackoverflow.com/questions/192249/how-do-i-parse-command-line-arguments-in-bash

UPDATE=0;
HELP=0;
LOG=0;
SHOW_TIME=0;
FLAG="";

while [ $# -gt 0 ]
do
key="$1"

case $key in
    -h|--help)
    HELP=1;
    shift
    ;;
    -l|--log)
    LOG=1;
    FLAG="$FLAG -l ";
    shift
    ;;
    -t|--time)
    SHOW_TIME=1;
    FLAG="$FLAG -t ";
    shift
    ;;
    -u|--update)
    REPO="$2"
    shift
    shift
    ;;
    -c|--connection)
    CONNECTION="$2"
    shift
    shift
    ;;
    *)    # unknown option
    INPUT="$1";
    shift # past argument
    ;;
esac
done

# Initsial sequence if first time program is being used.

#mkdir -p ~.config/sgs/;
#cat > ~.config/sgs/config.yaml <<EOF
#
#EOF

if [ -z "$INPUT" ] && [ -z "$REPO" ]; 
then 
   msgNewLine;
   msgHeadline "Update all repos `date +'%Y-%m-%d'`;";
   msgNewLine; 
   find $CONFIG_PATH -type f -name "*.yml" -not -name "config*" -printf "%f\n" | cut -f 1 -d "." |xargs ./sgs.sh $FLAG -u 
else
    if ! [ -z "$REPO" ];
    then
    # Go to directory of the repo
    FILE="$CONFIG_PATH/$REPO.yml";
    REPO_PATH_TMP=`cat $FILE | grep path | cut -f 2 -d : | xargs;`;
    msgHeadline "Cecking $REPO_PATH_TMP";

    ### Needed for expansion of ~
    REPO_PATH="$(eval echo $REPO_PATH_TMP)";
    cd $REPO_PATH;

    # Update repo
    ## Push any changes from repo 
    git pull origin master 2> /dev/null > /dev/null;

    # Find any local changes 
    GIT_STATUS=`git status -s | wc -l`; 
    if [ $GIT_STATUS -gt 0 ]
    then
        msgInfo "Commited $GIT_STATUS changes.";
        git add .;
        git commit -m "Commited $GIT_STATUS changes." > /dev/null;
        git push > /dev/null 2> /dev/null;
    else
        msg "Nothing to commit";
    fi
    msg "Finish";
    msgNewLine;
fi
fi
